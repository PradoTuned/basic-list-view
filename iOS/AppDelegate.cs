﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using FreshMvvm;
using FreshTinyIoC;
using SQLite;

namespace BasicListView.iOS {
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate {
		public override bool FinishedLaunching(UIApplication app, NSDictionary options) {
			global::Xamarin.Forms.Forms.Init();

			var contactDataBaseAccessor = new ContactDatabase(FileAccessHelper.GetLocalFilePath((FileAccessHelper.CONTACTDB_FILENAME)));
			FreshIOC.Container.Register(contactDataBaseAccessor);

			LoadApplication(new App());

			return base.FinishedLaunching(app, options);
		}
	}
}

﻿using System;
using System.IO;
namespace BasicListView.iOS {
	public static class FileAccessHelper {
		public static readonly string CONTACTDB_FILENAME = "contacts.db3";

		public static string GetLocalFilePath(string fileName) {
			// User the SpecialFolder enum to get the Personal folder on the Android file system.
			// Storing the database here is a best practice.
			string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			return Path.Combine(path, fileName);
		}
	}
}

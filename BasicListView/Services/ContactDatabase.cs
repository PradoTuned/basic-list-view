﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

namespace BasicListView {
	public class ContactDatabase {
		private readonly SQLiteAsyncConnection _dataBaseConnection;

		public string StatusMessage { get; set; }

		public ContactDatabase(string dataBasePathToUse) {
			_dataBaseConnection = new SQLiteAsyncConnection(dataBasePathToUse);
			_dataBaseConnection.CreateTableAsync<Contact>().Wait();
		}

		public async Task CreateContact(Contact contactToCreateOrUpdate) {
			try {
				// Basic validation to ensure we have a contact name.
				if (string.IsNullOrWhiteSpace(contactToCreateOrUpdate.Name))
					throw new Exception("Name is required");

				// Insert/update contact.
				var result = await _dataBaseConnection.InsertAsync(contactToCreateOrUpdate);
				if (result != 0) {
					await _dataBaseConnection.UpdateAsync(contactToCreateOrUpdate);
				}

				StatusMessage = $"{result} record(s) added [Contact Name: {contactToCreateOrUpdate.Name}])";
			} catch (Exception ex) {
				StatusMessage = $"Failed to create contact: {contactToCreateOrUpdate.Name}. Error: {ex.Message}";
			}
		}

		public Task<List<Contact>> GetListOfAllContactsAsync() {
			// Return a list of bills saved to the Bill table in the database.
			return _dataBaseConnection.Table<Contact>().ToListAsync();
		}

		public async Task<bool> DeleteContact(Contact contactToDelete) {
			try {
				await _dataBaseConnection.DeleteAsync(contactToDelete);
				return true;
			} catch {
				return false;
			}
		}
	}
}
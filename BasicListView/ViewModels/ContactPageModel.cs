﻿using System;
using FreshMvvm;
using System.Windows.Input;
using Xamarin.Forms;
namespace BasicListView {
	public class ContactPageModel : FreshBasePageModel {
		// Use IoC to get the accessor to the data base
		private ContactDatabase _contactDataBase = FreshIOC.Container.Resolve<ContactDatabase>();

		// Backing data model
		private Contact _contact;


		/// <summary>
		/// Gets or sets the name of the contact. Used for View binding.
		/// </summary>
		/// <value>The name of the contact.</value>
		public string ContactName {
			get { return _contact.Name; }
			set {
				if (_contact.Name != value) {
					_contact.Name = value;
					RaisePropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the contact email. Used for View binding.
		/// </summary>
		/// <value>The contact email.</value>
		public string ContactEmail {
			get { return _contact.Email; }
			set {
				if (_contact.Email != value) {
					_contact.Email = value;
					RaisePropertyChanged();
				}
			}
		}


		public ContactPageModel() {
		}

		/// <summary>
		/// Called whenever the page is navigated to.
		/// Either use a supplied Contact, or create a new one if not supplied.
		/// FreshMVVM does not provide a RaiseAllPropertyChanged,
		/// so we do this for each bound property, room for improvement.
		/// </summary>
		public override void Init(object initData) {
			_contact = initData as Contact;
			if (_contact == null) _contact = new Contact();

			base.Init(initData);

			RaisePropertyChanged(ContactName);
			RaisePropertyChanged(ContactEmail);
		}

		public ICommand SaveCommand {
			get {
				return new Command(async () => {
					if (_contact.IsValid()) {
						await _contactDataBase.CreateContact((_contact));
						await CoreMethods.PopPageModel(_contact);
					}
				});
			}
		}
	}
}

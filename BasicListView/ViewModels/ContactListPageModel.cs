﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using FreshMvvm;
using Xamarin.Forms;
namespace BasicListView {
	public class ContactListPageModel : FreshBasePageModel {
		private ContactDatabase _contactDataBase = FreshIOC.Container.Resolve<ContactDatabase>();

		public ObservableCollection<Contact> ListOfContacts { get; private set; }

		private Contact _selectedContact = null;
		public Contact SelectedContact {
			get { return _selectedContact; }
			set {
				_selectedContact = value;
				if (value != null) EditContactCommand.Execute(value);
			}
		}

		public ContactListPageModel() {
			ListOfContacts = new ObservableCollection<Contact>();
		}

		public override void Init(object initData) {
			LoadContacts();
			if (ListOfContacts.Count() < 1)
				CreateSampleData();
		}

		public override void ReverseInit(object returnedData) {
			LoadContacts();
			base.ReverseInit(returnedData);
		}

		public ICommand AddContactCommand {
			get {
				return new Command(async () => await CoreMethods.PushPageModel<ContactPageModel>());
			}
		}

		public ICommand EditContactCommand {
			get {
				return new Command(async (contact) => await CoreMethods.PushPageModel<ContactPageModel>(contact));
			}
		}

		private void LoadContacts() {
			ListOfContacts.Clear();
			Task<List<Contact>> getContactsTask = _contactDataBase.GetListOfAllContactsAsync();
			getContactsTask.Wait();

			foreach (var contact in getContactsTask.Result) {
				ListOfContacts.Add(contact);
			}
		}

		private void CreateSampleData() {
			var contact1 = new Contact {
				Name = "Jake Smith",
				Email = "jake.smith@mailmail.com"
			};

			var contact2 = new Contact {
				Name = "Jane Smith",
				Email = "jane.smith@mailmail.com"
			};

			var contact3 = new Contact {
				Name = "Jim Bob",
				Email = "jim.bob@mailmail.com"
			};

			var task1 = _contactDataBase.CreateContact(contact1);
			var task2 = _contactDataBase.CreateContact(contact2);
			var task3 = _contactDataBase.CreateContact(contact3);

			var allTasks = Task.WhenAll(task1, task2, task3);
			allTasks.Wait();

			LoadContacts();
		}
	}
}
